/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.dao;
import com.esprit.govoyage.handler.*;
import com.esprit.govoyage.entities.*;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
/**
 *
 * @author Lenovo-
 */
public class EvenementDAO {
    Evenement[] events;
    Lieu[] lieu;
    
    
    public boolean insert(Evenement event ){
        try {
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/pidev/insert_Event_GV.php?libelle="+event.getLibelle()+"&datedebut="+event.getDatedebut()+"&proprietes="+event.getPropriete());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
           StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    public boolean update(Evenement event ){
        try {
            System.out.println("http://localhost/pidev/update_Event_GV.php?libelle="+event.getLibelle()+"&datedebut="+event.getDatedebut()+"&proprietes="+event.getPropriete()+"&id="+event.getIdevenement());
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/pidev/update_Event_GV.php?libelle="+event.getLibelle()+"&datedebut="+event.getDatedebut()+"&proprietes="+event.getPropriete()+"&id="+event.getIdevenement());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
     public boolean Delete(int id){
        try {
            System.out.println("http://localhost/pidev/delete_Event_GV.php?&id="+id);
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/pidev/delete_Event_GV.php?&id="+id);
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
   public Evenement[] select(){
       try {
            EvenementHandler eventHandler = new EvenementHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/pidev/select_Event_GV.php");//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, eventHandler);
            // display the result
            events = eventHandler.getEvent();
             return events;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

             return null;
   }
    public Lieu[] selectLieu(int id){
       try {
            LieuHandler lieuHandler = new LieuHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/pidev/select_Lieu_GV.php?&id="+id);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, lieuHandler);
            // display the result
            lieu = lieuHandler.getLieu();
             return lieu;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

             return null;
   }
    
    
}
