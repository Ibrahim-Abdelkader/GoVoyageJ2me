/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.dao;
import com.esprit.govoyage.handler.*;
import com.esprit.govoyage.entities.*;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import com.esprit.govoyage.handler.*;
/**
 *
 * @author Lenovo-
 */
public class RestaurantDAO {
    Restaurant[] restos;
    
    public boolean insert(Restaurant resto ){
        try {
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/pidev/insert_Resto.php?nom="+resto.getNom()+"&description="+resto.getDescription()+"&adresse="+resto.getAdresse());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
           StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    public boolean update(Restaurant resto ){
        try {
            System.out.println("http://localhost/pidev/update_Resto.php?nom="+resto.getNom()+"&description="+resto.getDescription()+"&adresse="+resto.getAdresse()+"&id="+resto.getIdrestaurant());
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/pidev/update_Resto.php?nom="+resto.getNom()+"&description="+resto.getDescription()+"&adresse="+resto.getAdresse()+"&id="+resto.getIdrestaurant());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
         public boolean Delete(int id){
        try {
            System.out.println("http://localhost/pidev/delete_Resto.php?&id="+id);
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/pidev/delete_Resto.php?&id="+id);
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
   public Restaurant[] select(){
       try {
            restaurantHandlerN eventHandler = new restaurantHandlerN();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/pidev/select_Resto.php");//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, eventHandler);
            // display the result
            restos = eventHandler.getRestaurant();
            System.out.println(restos[1].getNom());
            System.out.println(restos);
            System.out.println("FF");
             return restos;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

             return null;
   }
    
    
}
