/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.dao;
import com.esprit.govoyage.entities.Ville;
import com.esprit.govoyage.handler.*;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
/**
 *
 * @author Lenovo-
 */
public class VilleDAO {
    Ville[] events;
    
    public boolean insert(Ville event ){
        try {
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/pidev/insert_Ville.php?libelle="+event.getLibelle()+"&pays="+event.getPays()+"&description="+event.getDescription()+"&aeroport="+event.getAeroport());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
           StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    public boolean update(Ville event ){
        try {
            System.out.println("http://localhost/pidev/update_Ville.php?libelle="+event.getLibelle()+"&pays="+event.getPays()+"&description="+event.getDescription()+"&aeroport="+event.getAeroport());
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/pidev/update_Ville.php?libelle="+event.getLibelle()+"&PAYS="+event.getPays()+"&descriptions="+event.getDescription()+"&aeroport="+event.getAeroport());
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
    public boolean Delete(int id){
        try {
            System.out.println("http://localhost/pidev/delete_Ville.php?&idville="+id);
            HttpConnection hc = (HttpConnection)Connector.open("http://localhost/pidev/delete_Ville.php?&idville="+id);
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
           int ch;
            while ((ch = dis.read())!=-1) {
                sb.append((char)ch);                
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
   public Ville[] select(){
       try {
            VilleHandler eventHandler = new VilleHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/pidev/select_Ville.php");//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, eventHandler);
            // display the result
            events = eventHandler.getEvent();
             return events;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

             return null;
   }
    
    
}
