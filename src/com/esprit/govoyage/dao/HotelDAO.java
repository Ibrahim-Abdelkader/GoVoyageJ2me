/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.dao;

import com.esprit.govoyage.entities.Hotel;
import com.esprit.govoyage.handler.HotelHandler;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author damine
 */
public class HotelDAO {

    Hotel[] hotels;

    public Hotel[] select() {
        try {
            HotelHandler hotelHandler = new HotelHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/pidev/select.php");//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, hotelHandler);
            // display the result
            hotels = hotelHandler.getHotel();
            return hotels;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public Hotel[] find(String nomHotel) {
        try {
            HotelHandler hotelHandler = new HotelHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/pidev/hotel_recherche_by_nom.php?nomHotel=" + nomHotel);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());

            SAXparser.parse(dis, hotelHandler);
            // display the result
            hotels = hotelHandler.getHotel();
            return hotels;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public void remove(int idHotel) {
        try {
            HotelHandler hotelHandler = new HotelHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/pidev/hotel_supprimer_by_idHotel.php?idHotel=" + idHotel);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());

            SAXparser.parse(dis, hotelHandler);

            //  Midlet.INSTANCE.disp.setCurrent(alert, new ChercherForum("Cherche Forum"));
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public Hotel[] select(int id) {
        try {
            HotelHandler hotelHandler = new HotelHandler();
            // get a parser object
            SAXParser SAXparser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/pidev/select_hotel_by_id.php?id=" + id);//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            SAXparser.parse(dis, hotelHandler);
            // display the result
            hotels = hotelHandler.getHotel();
            return hotels;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public boolean Update(Hotel hotel) {
        try {
            System.out.println(
                    "http://localhost/pidev/update_hotel.php?id="
                    + hotel.getId() + "&adresse="
                    + hotel.getAdresse() +  "&email=" + hotel.getEmail()
                    + "&nom =" + hotel.getNom()
                   
            
            );

            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/pidev/update_hotel.php?id="
                    +hotel.getId()+ "&adresse="
                    +hotel.getAdresse()+"&email=" +hotel.getEmail()
                    +"&nom=" +hotel.getNom()
            
                   
            );
            
            
            //
            
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            StringBuffer sb = new StringBuffer();
            int ch;
            while ((ch = dis.read()) != -1) {
                sb.append((char) ch);
            }
            if (sb.toString().trim().equals("success")) {
                return true;
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return false;
    }

}
