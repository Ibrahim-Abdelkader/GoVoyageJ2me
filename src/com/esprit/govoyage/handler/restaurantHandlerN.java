/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package  com.esprit.govoyage.handler;
import com.esprit.govoyage.entities.* ;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Lenovo-
 */
public class restaurantHandlerN extends DefaultHandler {

    private Vector restaurantVector;

    public restaurantHandlerN() {
        restaurantVector = new Vector();
    }

    public Restaurant[] getRestaurant() {
        Restaurant[] restaurantTab = new Restaurant[restaurantVector.size()];
        restaurantVector.copyInto(restaurantTab);
        return restaurantTab;
    }

    String selectedBalise = "";
    Restaurant seclectedRestaurant;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("restaurant")) {
            seclectedRestaurant = new Restaurant();
        } else if (qName.equals("idrestaurant")) {
            selectedBalise = "idrestaurant";
        } else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } else if (qName.equals("description")) {
            selectedBalise = "description";
        } else if (qName.equals("adresse")) {
            selectedBalise = "adresse";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("restaurant")) {
             restaurantVector.addElement(seclectedRestaurant);
            seclectedRestaurant = null;
        } else if (qName.equals("idrestaurant")) {
            selectedBalise = "";
        } else if (qName.equals("nom")) {
            selectedBalise = "";
        } else if (qName.equals("description")) {
            selectedBalise = "";
        } else if (qName.equals("adresse")) {
            selectedBalise = "";
        }
        
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedRestaurant != null) {
            if (selectedBalise.equals("idrestaurant")) {
                seclectedRestaurant.setIdrestaurant(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nom")) {
                seclectedRestaurant.setNom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("description")) {
                seclectedRestaurant.setDescription(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("adresse")) {
                seclectedRestaurant.setAdresse(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
        }
    }

}