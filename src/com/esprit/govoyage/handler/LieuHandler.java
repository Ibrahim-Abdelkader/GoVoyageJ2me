/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.handler;
import com.esprit.govoyage.entities.Lieu;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Lenovo-
 */
public class LieuHandler extends DefaultHandler {

    private Vector lieuVector;

    public LieuHandler() {
        lieuVector = new Vector();
    }

    public Lieu[] getLieu() {
        Lieu[] personTab = new Lieu[lieuVector.size()];
        lieuVector.copyInto(personTab);
        return personTab;
    }

    String selectedBalise = "";
    Lieu seclectedLieu;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("lieu")) {
            seclectedLieu = new Lieu();
        } else if (qName.equals("idlieu")) {
            selectedBalise = "idlieu";
        } else if (qName.equals("Cx")) {
            selectedBalise = "Cx";
        } else if (qName.equals("Cy")) {
            selectedBalise = "Cy";
        }else if (qName.equals("Cz")) {
            selectedBalise = "Cz";
        }else if (qName.equals("libelle")) {
            selectedBalise = "libelle";
        }else if (qName.equals("description")) {
            selectedBalise = "description";
        }else if (qName.equals("ville")) {
            selectedBalise = "ville";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("lieu")) {

            lieuVector.addElement(seclectedLieu);
            seclectedLieu = null;
        } else if (qName.equals("idlieu")) {
            selectedBalise = "";
        } else if (qName.equals("Cx")) {
            selectedBalise = "";
        }else if (qName.equals("Cy")) {
            selectedBalise = "";
        }else if (qName.equals("Cz")) {
            selectedBalise = "";
        }else if (qName.equals("libelle")) {
            selectedBalise = "";
        } else if (qName.equals("description")) {
            selectedBalise = "";
        }else if (qName.equals("ville")) {
            selectedBalise = "";
        }
        
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedLieu != null) {
            if (selectedBalise.equals("idlieu")) {
                seclectedLieu.setIdlieu(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("Cx")) {
                seclectedLieu.setCx(Double.parseDouble(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("Cy")) {
                seclectedLieu.setCy(Double.parseDouble(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("Cz")) {
                seclectedLieu.setCz(Double.parseDouble(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("libelle")) {
                seclectedLieu.setLibelle(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("description")) {
                seclectedLieu.setDescription(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("ville")) {
                seclectedLieu.setVille(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
        }
    }

}
