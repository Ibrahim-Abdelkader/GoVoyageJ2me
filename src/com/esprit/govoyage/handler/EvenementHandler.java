/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.handler;
import com.esprit.govoyage.entities.Evenement;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Lenovo-
 */
public class EvenementHandler extends DefaultHandler {

    private Vector evenementVector;

    public EvenementHandler() {
        evenementVector = new Vector();
    }

    public Evenement[] getEvent() {
        Evenement[] personTab = new Evenement[evenementVector.size()];
        evenementVector.copyInto(personTab);
        return personTab;
    }

    String selectedBalise = "";
    Evenement seclectedEvent;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("evenement")) {
            seclectedEvent = new Evenement();
        } else if (qName.equals("idevenement")) {
            selectedBalise = "idevenement";
        } else if (qName.equals("libelle")) {
            selectedBalise = "libelle";
        } else if (qName.equals("datedebut")) {
            selectedBalise = "datedebut";
        }else if (qName.equals("prix")) {
            selectedBalise = "prix";
        }else if (qName.equals("description")) {
            selectedBalise = "description";
        }else if (qName.equals("proprietes")) {
            selectedBalise = "proprietes";
        }else if (qName.equals("propevent")) {
            selectedBalise = "propevent";
        }else if (qName.equals("lieu")) {
            selectedBalise = "lieu";
        }else if (qName.equals("URL")) {
            selectedBalise = "URL";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("evenement")) {

            evenementVector.addElement(seclectedEvent);
            seclectedEvent = null;
        } else if (qName.equals("idevenement")) {
            selectedBalise = "";
        } else if (qName.equals("libelle")) {
            selectedBalise = "";
        }else if (qName.equals("datedebut")) {
            selectedBalise = "";
        }else if (qName.equals("prix")) {
            selectedBalise = "";
        }else if (qName.equals("description")) {
            selectedBalise = "";
        } else if (qName.equals("proprietes")) {
            selectedBalise = "";
        }else if (qName.equals("propevent")) {
            selectedBalise = "";
        }else if (qName.equals("lieu")) {
            selectedBalise = "";
        }else if (qName.equals("URL")) {
            selectedBalise = "";
        }
        
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedEvent != null) {
            if (selectedBalise.equals("idevenement")) {
                seclectedEvent.setIdevenement(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("libelle")) {
                seclectedEvent.setLibelle(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("datedebut")) {
                seclectedEvent.setDatedebut(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("prix")) {
                seclectedEvent.setPrix(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("description")) {
                seclectedEvent.setDescription(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("proprietes")) {
                seclectedEvent.setPropriete(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("propevent")) {
                seclectedEvent.setPropevent(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("lieu")) {
                seclectedEvent.setLieu(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("URL")) {
                seclectedEvent.setURL(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
        }
    }

}
