/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.govoyage.handler;

import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.esprit.govoyage.entities.Hotel;


/**
 *
 * @author user
 */
public class HotelHandler extends DefaultHandler {
    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




    private Vector hotelVector;

    public HotelHandler() {
        hotelVector = new Vector();
    }
    

  

    public Hotel[] getHotel() {
        Hotel[] personTab = new Hotel[hotelVector.size()];
        hotelVector.copyInto(personTab);
        return personTab;
    }

    String selectedBalise = "";
    Hotel seclectedMatch;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("Hotel")) {
            seclectedMatch = new Hotel();
        } else if (qName.equals("idhotel")) {
            selectedBalise = "idhotel";
        } 
        else if (qName.equals("idsh")) {
            selectedBalise = "idsh";
        }
        else if (qName.equals("idville")) {
            selectedBalise = "idville";
        } 
        else if (qName.equals("nom")) {
            selectedBalise = "nom";
        } 
        else if (qName.equals("nbretoiles")) {
            selectedBalise = "nbretoiles";
        } 
        else if (qName.equals("adresse")) {
            selectedBalise = "adresse";
        } 
        else if (qName.equals("email")) {
            selectedBalise = "email";
        } 
        else if (qName.equals("nomweb")) {
            selectedBalise = "nomweb";
        } 
        else if (qName.equals("nbrChambres")) {
            selectedBalise = "nbrChambres";
        } 
        else if (qName.equals("description")) {
            selectedBalise = "description";
        } 
        else if (qName.equals("imagehotel")) {
            selectedBalise = "imagehotel";
        } 
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("Hotel")) {

            hotelVector.addElement(seclectedMatch);
            seclectedMatch = null;
              } else if (qName.equals("idhotel")) {
            selectedBalise = "";
        } 
        else if (qName.equals("idsh")) {
            selectedBalise = "";
        }
        else if (qName.equals("idville")) {
            selectedBalise = "";
        } 
        else if (qName.equals("nom")) {
            selectedBalise = "";
        } 
        else if (qName.equals("nbretoiles")) {
            selectedBalise = "";
        } 
        else if (qName.equals("adresse")) {
            selectedBalise = "";
        } 
        else if (qName.equals("email")) {
            selectedBalise = "";
        } 
        else if (qName.equals("nomweb")) {
            selectedBalise = "";
        } 
        else if (qName.equals("nbrChambres")) {
            selectedBalise = "";
        } 
        else if (qName.equals("description")) {
            selectedBalise = "";
        } 
        else if (qName.equals("imagehotel")) {
            selectedBalise = "";
        } 
    
        
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedMatch != null) {
            if (selectedBalise.equals("idhotel")) {
                seclectedMatch.setId(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }       
            if (selectedBalise.equals("nom")) {
                seclectedMatch.setNom(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("nbretoiles")) {
                seclectedMatch.setNbreEtoils(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("adresse")) {
                seclectedMatch.setAdresse(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
                if (selectedBalise.equals("imagehotel")) {
                seclectedMatch.setImagehotel(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
                    if (selectedBalise.equals("nomweb")) {
                seclectedMatch.setNomweb(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
                if (selectedBalise.equals("description")) {
                seclectedMatch.setDescription(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
                if (selectedBalise.equals("idville")) {
                seclectedMatch.setIdVille(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
                if (selectedBalise.equals("email")) {
                seclectedMatch.setEmail(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
                 if (selectedBalise.equals("nomweb")) {
                seclectedMatch.setNomweb(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
                   if (selectedBalise.equals("idsh")) {
                seclectedMatch.setIdsh (Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
                    if (selectedBalise.equals("nbrChambres")) {
                seclectedMatch.setNbrChambres(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
                
                
                
                
                 
                 
             
        }
    }
    }}


