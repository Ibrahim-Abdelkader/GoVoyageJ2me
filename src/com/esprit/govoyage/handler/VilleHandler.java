/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.handler;
import com.esprit.govoyage.entities.Ville;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Lenovo-
 */
public class VilleHandler extends DefaultHandler {

    private Vector evenementVector;

    public VilleHandler() {
        evenementVector = new Vector();
    }

    public Ville[] getEvent() {
        Ville[] personTab = new Ville[evenementVector.size()];
        evenementVector.copyInto(personTab);
        return personTab;
    }

    String selectedBalise = "";
    Ville seclectedEvent;

    public void startElement(String string, String string1, String qName, Attributes atrbts) throws SAXException {
        if (qName.equals("ville")) {
            seclectedEvent = new Ville();
        } else if (qName.equals("idville")) {
            selectedBalise = "idville";
        } else if (qName.equals("libelle")) {
            selectedBalise = "libelle";
        } else if (qName.equals("pays")) {
            selectedBalise = "pays";
        } else if (qName.equals("description")) {
            selectedBalise = "description";
       }else if (qName.equals("aeroport")) {
            selectedBalise = "aeroport";
        }
    }

    public void endElement(String string, String string1, String qName) throws SAXException {
        if (qName.equals("ville")) {

            evenementVector.addElement(seclectedEvent);
            seclectedEvent = null;
        } else if (qName.equals("idville")) {
            selectedBalise = "";
        } else if (qName.equals("libelle")) {
            selectedBalise = "";
        } else if (qName.equals("pays")) {
            selectedBalise = "";
        } else if (qName.equals("description")) {
            selectedBalise = "";
        }else if (qName.equals("aeroport")) {
            selectedBalise = "";
        }
        
    }

    public void characters(char[] chars, int i, int i1) throws SAXException {
        if (seclectedEvent != null) {
            if (selectedBalise.equals("idville")) {
                seclectedEvent.setIdville(Integer.parseInt(new String(chars, i, i1)));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("libelle")) {
                seclectedEvent.setLibelle(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("pays")) {
                seclectedEvent.setPays(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("description")) {
                seclectedEvent.setDescription(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
            if (selectedBalise.equals("aeroport")) {
                seclectedEvent.setAeroport(new String(chars, i, i1));
                System.out.println(new String(chars, i, i1));
            }
        }
    }

}
