/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.entities;

/**
 *
 * @author Lenovo-
 */
public class Ville {
    
    private int idville;
    private String libelle;
    private String pays;
    private String description;
    private String aeroport;  
    

    public Ville(int idville, String libelle, String pays, String description, String aeroport) {
        this.idville = idville;
        this.libelle = libelle;
        this.pays = pays;
        this.description = description;
        this.aeroport = aeroport;
    }
    public Ville(String libelle, String pays, String description, String aeroport)
    {
        this.libelle = libelle;
        this.pays = pays;
        this.description = description;
        this.aeroport = aeroport;
    }

    public Ville() {
        
    }

    public int getIdville() {
        return idville;
    }

    public void setIdville(int idville) {
        this.idville = idville;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAeroport() {
        return aeroport;
    }

    public void setAeroport(String aeroport) {
        this.aeroport = aeroport;
    }

    

}

   

    
