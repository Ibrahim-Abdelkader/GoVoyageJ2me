/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.entities;

/**
 *
 * @author Lenovo-
 */
public class Restaurant {
    
    private int idrestaurant ;
    private String nom ;
    private String description ;
    private String adresse;

    public Restaurant(int idrestaurant, String nom, String description, String adresse) {
        this.idrestaurant = idrestaurant;
        this.nom = nom;
        this.description = description;
        this.adresse = adresse;
    }

    public Restaurant(String nom, String description, String adresse) {
        this.nom = nom;
        this.description = description;
        this.adresse = adresse;
    }

    public Restaurant() {
        
    }

   

    public int getIdrestaurant() {
        return idrestaurant;
    }

    public void setIdrestaurant(int idrestaurant) {
        this.idrestaurant = idrestaurant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
    


}
