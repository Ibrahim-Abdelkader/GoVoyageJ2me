/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.govoyage.entities;


public class Hotel {
    private int id;
    
    private String nom;
    private int nbreEtoils;
    private String adresse;
    private String imagehotel ;
    private String email;
    private String nomweb;
    private String description;
    private int idville;
    private int idsh ;
    private int nbrChambres;

    public Hotel(int id) {
        this.id = id;
    }

    public Hotel(int id, String nom, String adresse, String email) {
        this.id = id;
        this.nom = nom;
        this.adresse = adresse;
        this.email = email;
    }
    


    public Hotel() {
    }

    public Hotel(int id, String nom, int nbreEtoils, String adresse, String imagehotel, String email, String nomweb, String description) {
        this.id = id;
        this.nom = nom;
        this.nbreEtoils = nbreEtoils;
        this.adresse = adresse;
        this.imagehotel = imagehotel;
        this.email = email;
        this.nomweb = nomweb;
        this.description = description;
        
    }
    public Hotel(int id, String nom, int nbreEtoils, String adresse, String imagehotel, String email, String nomweb, String description, int idVille) {
        this.id = id;
        this.nom = nom;
        this.nbreEtoils = nbreEtoils;
        this.adresse = adresse;
        this.imagehotel = imagehotel;
        this.email = email;
        this.nomweb = nomweb;
        this.description = description;
        this.idville = idVille;
    }

    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNbreEtoils() {
        return nbreEtoils;
    }

    public void setNbreEtoils(int nbreEtoils) {
        this.nbreEtoils = nbreEtoils;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getImagehotel() {
        return imagehotel;
    }

    public void setImagehotel(String imagehotel) {
        this.imagehotel = imagehotel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomweb() {
        return nomweb;
    }

    public void setNomweb(String nomweb) {
        this.nomweb = nomweb;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdVille() {
        return idville;
    }

    public void setIdVille(int idVille) {
        this.idville = idVille;
    }

    public int getIdsh() {
        return idsh;
    }

    public void setIdsh(int idsh) {
        this.idsh = idsh;
    }

    public int getNbrChambres() {
        return nbrChambres;
    }

    public void setNbrChambres(int nbrChambres) {
        this.nbrChambres = nbrChambres;
    }

    public String toString() {
        return "Hotel :  nom=" + nom + ", nbreEtoils=" + nbreEtoils + ", adresse=" + adresse + ", imagehotel=" + imagehotel + ", email=" + email + ", nomweb=" + nomweb + ", description=" + description + ", idVille=" + idville + ", idsh=" + idsh + ", nbrChambres=" + nbrChambres + '}';
    }
    
    

  
    
    
}
