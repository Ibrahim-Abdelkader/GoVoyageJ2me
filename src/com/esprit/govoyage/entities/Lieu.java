/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.entities;

/**
 *
 * @author Lenovo-
 */
public class Lieu {
    private int idlieu;
    private double Cx;
    private double Cy;
    private double Cz;
    private String libelle;
    private String description;
     private int ville;

    public Lieu() {
    }

    public Lieu(int idlieu) {
        this.idlieu = idlieu;
    }

    public Lieu(int idlieu, double Cx, double Cy) {
        this.idlieu = idlieu;
        this.Cx = Cx;
        this.Cy = Cy;
    }

    public int getIdlieu() {
        return idlieu;
    }

    public void setIdlieu(int idlieu) {
        this.idlieu = idlieu;
    }

    public double getCx() {
        return Cx;
    }

    public void setCx(double Cx) {
        this.Cx = Cx;
    }

    public double getCy() {
        return Cy;
    }

    public void setCy(double Cy) {
        this.Cy = Cy;
    }

    public double getCz() {
        return Cz;
    }

    public void setCz(double Cz) {
        this.Cz = Cz;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getVille() {
        return ville;
    }

    public void setVille(int ville) {
        this.ville = ville;
    }
    
    
}
