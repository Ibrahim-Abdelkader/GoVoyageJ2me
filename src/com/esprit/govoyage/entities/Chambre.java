/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.entities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author user
 */
public class Chambre {

    private int idchambre;
    private int idhotel;
    private int etage;
    private int nbrlits;
    private String type;
    private String nomhotel;

    public String getNomhotel() {
        return nomhotel;
    }

    public void setNomhotel(String nomhotel) {
        this.nomhotel = nomhotel;
    }

    public Chambre(int idchambre, int idhotel, int etage, int nbrlits, String type, String nomhotel) {
        this.idchambre = idchambre;
        this.idhotel = idhotel;
        this.etage = etage;
        this.nbrlits = nbrlits;
        this.type = type;
        this.nomhotel = nomhotel;
    }

    public Chambre(int idchambre, int idhotel, int etage, int nbrlits, String type) {
        this.idchambre = idchambre;
        this.idhotel = idhotel;
        this.etage = etage;
        this.nbrlits = nbrlits;
        this.type = type;
    }

    public int getIdchambre() {
        return idchambre;
    }

    public void setIdchambre(int idchambre) {
        this.idchambre = idchambre;
    }

    public int getIdhotel() {
        return idhotel;
    }

    public void setIdhotel(int idhotel) {
        this.idhotel = idhotel;
    }

    public int getEtage() {
        return etage;
    }

    public void setEtage(int etage) {
        this.etage = etage;
    }

    public int getNbrlits() {
        return nbrlits;
    }

    public void setNbrlits(int nbrlits) {
        this.nbrlits = nbrlits;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
