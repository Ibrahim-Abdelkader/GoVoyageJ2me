/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.entities;

/**
 *
 * @author Lenovo-
 */
public class Evenement {
    
    private int idevenement ;
    private int idville ;
    private String libelle ;
    private String datedebut ;
    private int prix ;
    private String description ;
    private int propevent ;
    private String propriete ;
    private int lieu ;
    private  String URL ;

    public Evenement(String libelle, String datedebut, String proprietes) {
        this.libelle = libelle;
        this.datedebut = datedebut;
        this.propriete = proprietes;
    }

    public Evenement(int idevenement, String libelle, String datedebut, String propriete) {
        this.idevenement = idevenement;
        this.libelle = libelle;
        this.datedebut = datedebut;
        this.propriete = propriete;
    }

    

    public Evenement(int idevenement, int idville, String libelle, String datedebut, int prix, String description, int propevent, int lieu, String propriete) {
        this.idevenement = idevenement;
        this.idville = idville;
        this.libelle = libelle;
        this.datedebut = datedebut;
        this.prix = prix;
        this.description = description;
        this.propevent = propevent;
        this.lieu = lieu;
        this.propriete = propriete;
    }

    public Evenement(String libelle, String datedebut, int prix, String description, int propevent, int lieu, String URL, String propriete) {
        this.libelle = libelle;
        this.datedebut = datedebut;
        this.prix = prix;
        this.description = description;
        this.propevent = propevent;
        this.lieu = lieu;
        this.URL = URL;
        this.propriete = propriete;
    }
   
    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public Evenement(int propevent, int lieu) {
        this.propevent = propevent;
        this.lieu = lieu;
    }

    public int getPropevent() {
        return propevent;
    }

    public void setPropevent(int propevent) {
        this.propevent = propevent;
    }

    public int getLieu() {
        return lieu;
    }

    public void setLieu(int lieu) {
        this.lieu = lieu;
    }

    public Evenement() {
    }

    public Evenement(int idevenement, int idville, String libelle, String datedebut, int prix, String description, String propriete) {
        this.idevenement = idevenement;
        this.idville = idville;
        this.libelle = libelle;
        this.datedebut = datedebut;
        this.prix = prix;
        this.description = description;
        this.propriete = propriete;
    }
   

    public Evenement(String libelle, String datedebut, int prix, String description, String propriete) {
        this.libelle = libelle;
        this.datedebut = datedebut;
        this.prix = prix;
        this.description = description;
        this.propriete = propriete;
    }

    public int getIdevenement() {
        return idevenement;
    }

    public void setIdevenement(int idevenement) {
        this.idevenement = idevenement;
    }

    public int getIdville() {
        return idville;
    }

    public void setIdville(int idville) {
        this.idville = idville;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(String datedebut) {
        this.datedebut = datedebut;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPropriete() {
        return propriete;
    }

    public void setPropriete(String propriete) {
        this.propriete = propriete;
    }
    
}
