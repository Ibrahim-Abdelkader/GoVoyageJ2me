/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;

import com.esprit.govoyage.test.Midlet;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author user
 */
public class ChercherHotel extends Form implements CommandListener, Runnable {

    TextField nomHotel = new TextField("nom Hotel", null, 250, TextField.ANY);
    Command cmdChercherForum = new Command("Chercher", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);

    public ChercherHotel(String title) {
        super(title);
        addCommand(cmdExit);
        addCommand(cmdChercherForum);
        setCommandListener(this);
        this.append(nomHotel);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdChercherForum) {
            Midlet.INSTANCE.disp.setCurrent(new HotelListResult(nomHotel.getString()));
        }
        if (c == cmdExit) {
            Midlet.INSTANCE.disp.setCurrent(new HotelList());
        }
    }

    public void run() {
    }

}
