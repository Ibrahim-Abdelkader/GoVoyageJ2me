/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;
import com.esprit.govoyage.*;
import com.esprit.govoyage.test.Midlet;
import com.esprit.govoyage.dao.RestaurantDAO;
import com.esprit.govoyage.entities.Restaurant;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author Lenovo-
 */
public class ListRestoForm extends List implements CommandListener,Runnable {

    Command cmdAjout = new Command("Ajouter un restaurant", Command.SCREEN, 0);
    Command cmdUpdate = new Command("Modifier un restaurant", Command.SCREEN, 0);
    Command cmdDelete = new Command("Supprimer un restaurantt", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);

Command cmdSms = new Command("Envoyer un sms", Command.SCREEN, 0);
    public ListRestoForm() {
        super("List Restaurants", List.IMPLICIT);
        addCommand(cmdAjout);
        addCommand(cmdExit);
        addCommand(cmdUpdate);
        addCommand(cmdSms);
        addCommand(cmdDelete);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
    }

   

    public void commandAction(Command c, Displayable d) {
        if (c == cmdAjout) {
            Midlet.INSTANCE.disp.setCurrent(new AddRestoForm());
        }
        if (c == cmdUpdate) {
            Midlet.INSTANCE.disp.setCurrent(new UpdateRestoForm());
        }
        if (c == cmdDelete) {
            Midlet.INSTANCE.disp.setCurrent(new AddRestoForm());
        }
        if (c == cmdSms) {
            Midlet.INSTANCE.disp.setCurrent(new SendSms());
        }
        if (c == cmdExit) {
            Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
        Restaurant [] restos = new RestaurantDAO().select();
        if (restos.length > 0) {
            for (int i = 0; i < restos.length; i++) {
                 append(restos[i].getNom()+ " - " + restos[i].getDescription()+" - "+restos[i].getAdresse(),null);
            }
        }
    }
}
