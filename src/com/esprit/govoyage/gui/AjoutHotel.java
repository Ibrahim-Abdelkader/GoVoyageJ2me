package com.esprit.govoyage.gui;

import com.esprit.govoyage.test.Midlet;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;

import javax.microedition.lcdui.TextField;

/**
 *
 * @author Rami
 */
public class AjoutHotel extends Form implements CommandListener {

    TextField tfNom = new TextField("Nom Hotel: ", "", 25, TextField.ANY);
    TextField tfAdresse = new TextField("Adresse: ", "", 25, TextField.ANY);
    TextField tfNbretoil = new TextField("Nombre Etoile: ", "", 25, TextField.ANY);

    TextField tfEmail = new TextField("Email:", "", 25, TextField.ANY);
    TextField tfWeb = new TextField("Site Web: ", "", 25, TextField.ANY);
    TextField tfDescription = new TextField("Description: ", "", 35, TextField.ANY);

    Command cmdNext = new Command("Ajouter", Command.OK, 0);
    Command cmdBack = new Command("Revenir", Command.BACK, 0);
    HttpConnection hc;
    DataInputStream dis;
    StringBuffer sb;
    String url = "http://localhost/pidev/insert.php?";

    public AjoutHotel() {
        super("Ajout");
        append(tfNom);
        append(tfAdresse);
        append(tfNbretoil);
        append(tfEmail);
        append(tfWeb);
        append(tfDescription);

        addCommand(cmdNext);
        addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new HotelList());

        }
        if (c == cmdNext) {
            try {
                String nom, adresse, nbreEtoils, email, web, description;

                nom = tfNom.getString();
                adresse = tfAdresse.getString();
                nbreEtoils = tfNbretoil.getString();
                email = tfEmail.getString();
                web = tfWeb.getString();

                description = tfDescription.getString();

                hc = (HttpConnection) Connector.
                        open(url + "tfNom=" + nom
                                + "&tfNbretoil=" + nbreEtoils
                                + "&tfEmail=" + email
                                + "&tfAdresse=" + adresse
                                + "&tfWeb=" + web
                                + "&tfDescription=" + description);
                dis = hc.openDataInputStream();

                int ascii;
                sb = new StringBuffer();

                while ((ascii = dis.read()) != -1) {

                    sb.append((char) ascii);
                }
                if (sb.toString().equals("successfully added")) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.CONFIRMATION);
                    a.setTimeout(3000);
                    Midlet.INSTANCE.disp.setCurrent(a);
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    Midlet.INSTANCE.disp.setCurrent(a);
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }
}
