/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;

import com.esprit.govoyage.test.Midlet;
import com.esprit.govoyage.dao.RestaurantDAO;
import com.esprit.govoyage.entities.Restaurant;
import java.util.Calendar;
import java.util.Vector;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.ItemStateListener;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Lenovo-
 */
public class UpdateRestoForm extends Form implements CommandListener, Runnable , ItemStateListener  {
    Restaurant [] restos = new RestaurantDAO().select();
    TextField nom = new TextField("nom du restaurant", "", 50, TextField.ANY);
    TextField description = new TextField("description du restaurant :", "", 50, TextField.ANY);
    TextField adresse = new TextField("adresse du restaurant :", "", 50, TextField.ANY);
    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdSuppression = new Command("Supprimer", Command.SCREEN, 0);
    Command cmdLoadList = new Command("load List", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);
    String[] tabJob = {};
    int id_resto ;
    ChoiceGroup Ch = new ChoiceGroup("choice group", ChoiceGroup.POPUP, tabJob, null);
     
    
   
    public UpdateRestoForm() {
        
        super("Restaurant");
        
        
        append(nom);
        append(description);
        append(adresse);
        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        addCommand(cmdSuppression);
        addCommand(cmdLoadList);
        append(Ch);
        setCommandListener(this);
        setItemStateListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new ListRestoForm());
        }
                if (c == cmdSuppression) {
           run_Delete();
            
        }
        if (c == cmdEnregistrer) {
            Thread th = new Thread(this);
            th.start();
        }
        if (c == cmdLoadList) {
              if (restos.length > 0) {
            for (int i = 0; i < restos.length; i++) {
             String S = restos[i].getNom() ;
             Ch.append(S, null);
            }
        }
        }

    }

    public void run() {
       
        String streq1 = nom.getString();
        String streq2 = description.getString();
        String streq3 = adresse.getString();
        boolean result = new RestaurantDAO().update(new Restaurant(id_resto,streq1,streq2,streq3));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Restaurant modifié avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert,new ListRestoForm());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("modification du restaurant échoué");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
    }
      
              public void run_Delete() {
       
        boolean result = new RestaurantDAO().Delete(id_resto);
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Restaurant supprime avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert,new ListRestoForm());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("suppression du restaurant échouée");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
       
    }
        
       
    

    public void itemStateChanged(Item item) {
        if(item==Ch){
        int id = 0 ;
       
        id = Ch.getSelectedIndex() ;
        id_resto = restos[id].getIdrestaurant();
            System.out.println("id resto"+id_resto);
        nom.setString(restos[id].getNom());
        description.setString(restos[id].getDescription());
        adresse.setString(restos[id].getAdresse());
        }
    }
}
