/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;

import com.esprit.govoyage.dao.HotelDAO;
import com.esprit.govoyage.entities.Hotel;
import com.esprit.govoyage.test.Midlet;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextBox;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author user
 */
public class ModifierHotel extends Form implements CommandListener, Runnable {

    Hotel[] hotel;
    Hotel[] hotelMod;
    Command cmdBack = new Command("Retour", Command.EXIT, 0);
    Command cmdModifier = new Command("Modifier", Command.SCREEN, 0);
    TextField nom = new TextField("nom hotel :", null, 250, TextField.ANY);
    TextField adresse = new TextField("adresse ;", null, 250, TextField.ANY);
    TextField email = new TextField("email ;", null, 250, TextField.ANY);
    TextField nomWeb = new TextField("nom web ;", null, 250, TextField.ANY);
    TextField description = new TextField("description ;", null, 250, TextField.ANY);
    TextField nbresEtoils = new TextField("Nbre etoils ;", null, 250, TextField.ANY);
    TextField nbreChambre = new TextField("Nbres chambres ;", null, 250, TextField.ANY);

    public ModifierHotel(String title, Hotel[] hotel) {
        super("mayssa");
        this.hotel = hotel;
        addCommand(cmdBack);
        addCommand(cmdModifier);
        append(nom);
        append(adresse);
        append(email);
        append(nbresEtoils);
        append(nbreChambre);

        setCommandListener(this);
        adresse.setString(hotel[1].getAdresse());
        nom.setString(hotel[1].getNom());
        email.setString(hotel[1].getEmail());
        nomWeb.setString(hotel[1].getNomweb());
        description.setString(hotel[1].getDescription());
        nbreChambre.setString(String.valueOf(hotel[1].getNbrChambres()));
        nbresEtoils.setString(String.valueOf(hotel[1].getNbreEtoils()));

    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new HotelList());
        }
        if (c == cmdModifier) {

            Thread th = new Thread(this);
            th.start();

        }

    }

    public void run() {

        hotel[1].setNom(nom.getString());
        hotel[1].setAdresse(adresse.getString());
        hotel[1].setEmail(email.getString());
        hotel[1].setId(hotel[1].getId());
        boolean result = new HotelDAO().Update(hotel[1]);

        Alert alert = new Alert("Résultat");
        Midlet.INSTANCE.disp.setCurrent(alert);
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Sujet modifier avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert);
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("modification  échouée");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }

    }

}
