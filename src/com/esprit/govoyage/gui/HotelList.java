package com.esprit.govoyage.gui;

import com.esprit.govoyage.dao.HotelDAO;
import com.esprit.govoyage.entities.Hotel;
import com.esprit.govoyage.test.Midlet;
import java.io.IOException;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.List;

public class HotelList extends List implements CommandListener, Runnable {

    Image img;

    Display disp;
    Command cmdAjout = new Command("Ajouter un nouveau hotel", Command.SCREEN, 0);
    Command cmdAfficherHotel = new Command("Details", Command.SCREEN, 0);
    Command cmdChercherHotel = new Command("Chercher", Command.SCREEN, 0);
    Command cmdModifierHotel = new Command("Modifier", Command.SCREEN, 0);
    Command cmdSupprimerHotel = new Command("Supprimer", Command.SCREEN, 0);

    Command cmdExit = new Command("Exit", Command.EXIT, 0);
    Hotel[] hotels;
    Hotel[] hotels2;

    public HotelList() {
        super("Liste Des Hotels", List.IMPLICIT);

        addCommand(cmdAjout);
        addCommand(cmdAfficherHotel);
        addCommand(cmdChercherHotel);
        addCommand(cmdModifierHotel);
        addCommand(cmdSupprimerHotel);

        addCommand(cmdExit);
        setCommandListener(this);
        try {
            img = Image.createImage("/com/esprit/govoyage/ressources/logo.jpg");
        } catch (IOException e) {
            Alert alert = new Alert("pff");
        }
        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmdExit) {
            Midlet.INSTANCE.notifyDestroyed();
        }
        if (c == cmdAjout) {
            Midlet.INSTANCE.disp.setCurrent(new AjoutHotel());

        }

        if (c == cmdChercherHotel) {
            Midlet.INSTANCE.disp.setCurrent(new ChercherHotel("rabbi m3ana"));

        }
        if (c == cmdAfficherHotel) {
            //System.out.println("iddd"+ hotels[getSelectedIndex()].getId());
            Midlet.INSTANCE.disp.setCurrent(new AfficherHotelsDetails(hotels[getSelectedIndex()].getId()));

        }

        if (c == cmdModifierHotel) {
            hotels2 = new HotelDAO().select(hotels[getSelectedIndex()].getId());
            Midlet.INSTANCE.disp.setCurrent(new ModifierHotel("aaa", hotels));

        }
        if (c == cmdSupprimerHotel) {
             new HotelDAO().remove(hotels[getSelectedIndex()].getId());
             Midlet.INSTANCE.disp.setCurrent(new HotelList());

        }
    }

    public void run() {
        hotels = new HotelDAO().select();
            for (int i = 0; i < hotels.length; i++) {
                append(hotels[i].getNom(), img);
            }
        
    }

}
