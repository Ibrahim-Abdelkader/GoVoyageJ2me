/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;

import com.esprit.govoyage.test.Midlet;
import com.esprit.govoyage.dao.EvenementDAO;
import com.esprit.govoyage.entities.Evenement;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import java.util.Calendar;

/**
 *
 * @author Lenovo-
 */
public class AddEvenetForm extends Form implements CommandListener, Runnable {

    TextField libelle = new TextField("nom de l'evenement : ", "", 50, TextField.ANY);
    TextField proprietes = new TextField("proprietes de l'evenement :", "", 50, TextField.ANY);
    DateField datedebut = new DateField("date debut :",DateField.DATE);
    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);

    public AddEvenetForm() {
        super("Evenement");

        append(libelle);
        append(proprietes);
        append(datedebut);
        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new ListEvenetForm());
        }
        if (c == cmdEnregistrer) {
            Thread th = new Thread(this);
            th.start();
        }

    }

    public void run() {
        String streq1 = libelle.getString();
        String streq2 = proprietes.getString();
        Calendar cal= Calendar.getInstance();
        cal.setTime(datedebut.getDate());
        String date = cal.get(Calendar.YEAR) + "-" + ( cal.get(Calendar.MONTH)+1  ) + "-" + cal.get(Calendar.DAY_OF_MONTH);
      //  String strarb = datedebut.getDate().toString();
        System.out.println("la date est : "+date);
        boolean result = new EvenementDAO().insert(new Evenement(streq1,date,streq2));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Evenement ajouté avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert,new ListEvenetForm());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Evenement du match échoué");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
    }

}
