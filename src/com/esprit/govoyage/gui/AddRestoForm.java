/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;

import com.esprit.govoyage.test.Midlet;
import com.esprit.govoyage.dao.RestaurantDAO;
import com.esprit.govoyage.entities.Restaurant;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import java.util.Calendar;

/**
 *
 * @author Lenovo-
 */
public class AddRestoForm extends Form implements CommandListener, Runnable {

    TextField nom = new TextField("Nom du restaurant", "", 50, TextField.ANY);
    TextField description = new TextField("Description:", "", 50, TextField.ANY);
    TextField adresse = new TextField("Adresse :", "",50 , TextField.ANY);

    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);

    public AddRestoForm() {
        super("Restaurant");

        append(nom);
        append(description);
        append(adresse);
        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new ListRestoForm());
        }
        if (c == cmdEnregistrer) {
            Thread th = new Thread(this);
            th.start();
        }

    }

    public void run() {
        String streq1 = nom.getString();
        String streq2 = description.getString();
        String streq3 = adresse.getString();
        boolean result = new RestaurantDAO().insert(new Restaurant(streq1, streq2, streq3));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Restaurant ajouté avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert, new ListRestoForm());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Ajout du restaurant échoué");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
    }

}
