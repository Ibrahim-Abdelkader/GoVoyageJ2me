/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.govoyage.gui;

import com.esprit.govoyage.dao.HotelDAO;
import com.esprit.govoyage.entities.Hotel;
import com.esprit.govoyage.test.Midlet;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author user
 */
class HotelListResult extends List implements CommandListener,Runnable {
    int i=0;
    Command cmdExit = new Command("Retour", Command.EXIT, 0);
    Hotel[] hotels;
    String nomHotel;
    public HotelListResult(String nomHotel) {
        super("Resultat", List.IMPLICIT);
        addCommand(cmdExit);
        this.nomHotel = nomHotel;
        setCommandListener(this);

        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdExit) {
            Midlet.INSTANCE.disp.setCurrent(new HotelList());
        }
    }

    public void run() {
    try {
        System.out.println(nomHotel);
            hotels = new HotelDAO().find(nomHotel);
            if (hotels.length > 0) {
                i = i + 1;
                for (int i = 0; i < hotels.length; i++) {
                    append(hotels[i].toString(), null);
                }
            }
        } catch (Exception e) {
            Alert alert = new Alert("Résultat");
            alert.setType(AlertType.ERROR);
            alert.setString("Aucun resulat trouvé");
        }
    
    
    }

}
