/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.esprit.govoyage.gui;

import com.esprit.govoyage.dao.VilleDAO;
import com.esprit.govoyage.entities.Ville;
import com.esprit.govoyage.test.Midlet;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author Lenovo-
 */
public class ListVilleForm extends List implements CommandListener,Runnable {

    Command cmdAjout = new Command("Ajouter une ville", Command.SCREEN, 0);
    Command cmdUpdate = new Command("Modifier une ville", Command.SCREEN, 0);
    Command cmdDelete = new Command("Supprimer une ville", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);
 Ville [] events;

    public ListVilleForm() {
        super("List Villes", List.IMPLICIT);
        addCommand(cmdAjout);
        addCommand(cmdExit);
        addCommand(cmdUpdate);
        addCommand(cmdDelete);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
    }

   

    public void commandAction(Command c, Displayable d) {
        if (c == cmdAjout) {
            Midlet.INSTANCE.disp.setCurrent(new AddVilleForm());
        }
        if (c == cmdUpdate) {
            Midlet.INSTANCE.disp.setCurrent(new UpdateVilleForm());
        }
        if (c == cmdDelete) {
            VilleDAO ville = new VilleDAO();
            ville.Delete(events[getSelectedIndex()].getIdville());
            Midlet.INSTANCE.disp.setCurrent(new ListVilleForm());
        }
        if (c == cmdExit) {
            Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
        events = new VilleDAO().select();
        if (events.length > 0) {
            for (int i = 0; i < events.length; i++) {
                append(events[i].getLibelle() + " - " + events[i].getPays()+" - "+events[i].getDescription()+" - "+events[i].getAeroport(),null);
            }
        }
    }
}
