/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;



import com.esprit.govoyage.dao.VilleDAO;
import com.esprit.govoyage.entities.Ville;
import com.esprit.govoyage.test.Midlet;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.ItemStateListener;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Lenovo-
 */
public class UpdateVilleForm extends Form implements CommandListener, Runnable, ItemStateListener {

    Ville[] events = new VilleDAO().select();
    TextField libelle = new TextField("Nom de la ville", "", 50, TextField.ANY);
    TextField pays = new TextField("Pays :", "", 50, TextField.ANY);
    TextField description = new TextField("Description :", "", 50, TextField.ANY);
    TextField aeroport = new TextField("Aeroport :", "", 50, TextField.ANY);

    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);
    String[] tabJob = {};
    int id_ville;
    ChoiceGroup Ch = new ChoiceGroup("choice group", ChoiceGroup.POPUP, tabJob, null);

    public UpdateVilleForm() {

        super("Ville");

        if (events.length > 0) {
            for (int i = 0; i < events.length; i++) {
                String S = events[i].getLibelle();
                Ch.append(S, null);
            }
        }
        append(Ch);
        append(libelle);
        append(pays);
        append(description);
        append(aeroport);
        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        setCommandListener(this);
        setItemStateListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new ListVilleForm());
        }
        if (c == cmdEnregistrer) {
            Thread th = new Thread(this);
            th.start();
        }

    }

    public void run() {

        String streq1 = libelle.getString();
        String streq2 = pays.getString();
        String streq3 = description.getString();
        String streq4 = aeroport.getString();

        boolean result = new VilleDAO().update(new Ville(id_ville, streq1, streq2, streq3, streq4));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Modification echouee");
            Midlet.INSTANCE.disp.setCurrent(alert, new ListVilleForm());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Ville modifiée avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }

    }
    
    public void run_Delete() {
       
        boolean result = new VilleDAO().Delete(id_ville);
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Ville supprimée avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert,new ListVilleForm());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Suppression de la ville échouée");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
       
    }

    public void itemStateChanged(Item item) {
        if (item == Ch) {
            int id = 0;
            id_ville = events[id].getIdville();
            id = Ch.getSelectedIndex();
            libelle.setString(events[id].getLibelle());
            pays.setString(events[id].getPays());
            description.setString(events[id].getDescription());
            aeroport.setString(events[id].getAeroport());

        }
    }
}
