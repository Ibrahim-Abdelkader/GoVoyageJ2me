/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;

import com.esprit.govoyage.test.Midlet;
import com.esprit.govoyage.dao.EvenementDAO;
import com.esprit.govoyage.entities.Evenement;
import com.esprit.govoyage.entities.Lieu;
import java.util.Calendar;
import java.util.Vector;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.ItemStateListener;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Lenovo-
 */
public class UpdateEventForm extends Form implements CommandListener, Runnable , ItemStateListener  {
    Evenement [] events = new EvenementDAO().select();
    TextField libelle = new TextField("nom de l'evenement 1", "", 50, TextField.ANY);
    TextField proprietes = new TextField("proprietes de l'evenement :", "", 50, TextField.ANY);
    DateField datedebut = new DateField("date debut :",DateField.DATE);
    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdSuppression = new Command("Supprimer", Command.SCREEN, 0);
    Command cmdMap = new Command("Voir sur la Map", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);
    String[] tabJob = {};
    int id_ville ;
    int id_lieu;
    double lattitude ;
    double longitude;
    String label ;
    ChoiceGroup Ch = new ChoiceGroup("choice group", ChoiceGroup.POPUP, tabJob, null);
     
    
   
    public UpdateEventForm() {
        
        super("Evenement");
        
          if (events.length > 0) {
            for (int i = 0; i < events.length; i++) {
             String S = events[i].getLibelle() ;
             Ch.append(S, null);
             
            }
        }
        append(Ch);
        append(libelle);
        append(proprietes);
        append(datedebut);
        addCommand(cmdSuppression);
        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        addCommand(cmdMap);
        id_lieu = events[0].getLieu();
        setCommandListener(this);
        setItemStateListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new ListEvenetForm());
            
        }
        if (c == cmdSuppression) {
           run_Delete();
          
        }
         if (c == cmdMap) {
           run_Map() ;
           Midlet.INSTANCE.disp.setCurrent(new GoogleMapsMarkerCanvas(Midlet.INSTANCE, d, (int)lattitude, (int)longitude , label));
         }
        if (c == cmdEnregistrer) {
            Thread th = new Thread(this);
            th.start();
        }

    }

    public void run() {
       
        String streq1 = libelle.getString();
        String streq2 = proprietes.getString();
        Calendar cal= Calendar.getInstance();
        cal.setTime(datedebut.getDate());
        

        String date = cal.get(Calendar.YEAR) + "-" + ( cal.get(Calendar.MONTH)+1  ) + "-" + cal.get(Calendar.DAY_OF_MONTH);
      //  String strarb = datedebut.getDate().toString();
        System.out.println("la date est : "+date+streq1+streq2);
        boolean result = new EvenementDAO().update(new Evenement(id_ville,streq1,date,streq2));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Evenement modifié avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert,new ListEvenetForm());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("modification du evenement échoué");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
       
    }
      
    public void run_Delete() {
       
        boolean result = new EvenementDAO().Delete(id_ville);
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Evenement modifié avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert,new ListEvenetForm());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("modification du evenement échoué");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
       
    }
      public void run_Map() {
       
        Lieu [] lieuM = new EvenementDAO().selectLieu(id_lieu);
        lattitude = lieuM[0].getCx();
        longitude = lieuM[0].getCy();
        label = lieuM[0].getLibelle();
        System.out.println("lattitude"+lieuM[0].getCx());
        System.out.println("longitude"+ lieuM[0].getCy());
        
       
    }
    public void itemStateChanged(Item item) {
        if(item==Ch){
        int id = 0 ;
        
        id = Ch.getSelectedIndex() ;
        id_lieu = events[id].getLieu();
            System.out.println("id lieu :"+id_lieu);
        libelle.setString(events[id].getLibelle());
        proprietes.setString(events[id].getPropriete());
        proprietes.setString(events[id].getPropriete());
        String year = events[id].getDatedebut().substring(0, 4);
        String Month = events[id].getDatedebut().substring(5, 7);
        String day = events[id].getDatedebut().substring(8, 10);
        Calendar cale = Calendar.getInstance();
        cale.set(Calendar.YEAR,Integer.parseInt(year));
        cale.set(Calendar.MONTH,Integer.parseInt(Month));
        cale.set(Calendar.DAY_OF_MONTH,Integer.parseInt(day));
        datedebut.setDate(cale.getTime());
            System.out.println(" year : "+year+" Month : "+Month+" day : "+day);
        id_ville = events[id].getIdevenement();
        }
    }
}

