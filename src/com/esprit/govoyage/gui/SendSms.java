package com.esprit.govoyage.gui;

import com.esprit.govoyage.test.Midlet;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;

import javax.wireless.messaging.*;
public class SendSms extends Form implements CommandListener, Runnable  {
      Display display;
    //  private TextField toWhom;
      //private TextField message;
      private Alert alert;
     // private Command send,exit;
      MessageConnection clientConn;
//      compose;
     // private Form compose=new Form("Compose Message");
        TextField    toWhom=new TextField("To","",10,TextField.PHONENUMBER);
          TextField  message=new TextField("Message","",600,TextField.ANY);
          Command  send=new Command("Send",Command.BACK,0);
         Command   exit=new Command("Exit",Command.SCREEN,5);
         Command cmdBack = new Command("Back", Command.EXIT, 0);
      
      public SendSms() {
           super("Envoi SMS");
//            display=Display.getDisplay(this);
            
            append(toWhom);
           append(message);
            addCommand(send);
            addCommand(exit);
            addCommand(cmdBack);
            setCommandListener(this);
      }

    
//      public void startApp() {
//            display.setCurrent(compose);
//      }
//      public void pauseApp() {
//      }
//      public void destroyApp(boolean unconditional) {
//            notifyDestroyed();
//      }
      public void commandAction(Command cmd,Displayable disp) {
            if(cmd==exit) {
                 Midlet.INSTANCE.notifyDestroyed();}
            
            if(cmd==send) {
               Thread th=new Thread(this);
               th.start();
      } if (cmd == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new AddRestoForm());
        }
            
            
      }
      

    public void run() {
      String mno=toWhom.getString();
                  String msg=message.getString();
                  if(mno.equals("")) {
                        
                        alert.setString("Enter Mobile Number!!!");
                        alert.setTimeout(2000);
                        display.setCurrent(alert);
                  }
                  else {
                        try {
                              clientConn=(MessageConnection)Connector.open("sms://"+mno);
                        }
                        catch(Exception e) {
                              alert = new Alert("Alert");
                              alert.setString("Unable to connect to Station because of network problem");
                              alert.setTimeout(2000);
                              display.setCurrent(alert);
                        }
                        try {
                              TextMessage textmessage = (TextMessage) clientConn.newMessage(MessageConnection.TEXT_MESSAGE);
                              textmessage.setAddress("sms://"+mno);
                              textmessage.setPayloadText(msg);
                              clientConn.send(textmessage);
                        }
                        catch(Exception e)
                        {
                              Alert alert=new Alert("Alert","",null,AlertType.INFO);
                              alert.setTimeout(Alert.FOREVER);
                              alert.setString("Unable to send");
                              display.setCurrent(alert);
                        }
                  }
            }

}