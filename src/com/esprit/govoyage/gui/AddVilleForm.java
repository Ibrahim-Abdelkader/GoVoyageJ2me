/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;


import com.esprit.govoyage.dao.VilleDAO;
import com.esprit.govoyage.entities.Ville;
import com.esprit.govoyage.test.Midlet;
import java.util.Calendar;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Lenovo-
 */
public class AddVilleForm extends Form implements CommandListener, Runnable {

    TextField libelle = new TextField("Nom de la ville", "", 50, TextField.ANY);
    TextField pays = new TextField("Pays :", "", 50, TextField.ANY);
    TextField description = new TextField("Description :", "", 50, TextField.ANY);
    TextField aeroport = new TextField("Aeroport :", "", 50, TextField.ANY);

    Command cmdEnregistrer = new Command("Enregistrer", Command.SCREEN, 0);
    Command cmdBack = new Command("Back", Command.EXIT, 0);

    public AddVilleForm() {
        super("Ville");

        append(libelle);
        append(pays);
        append(description);
        append(aeroport);

        addCommand(cmdEnregistrer);
        addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            Midlet.INSTANCE.disp.setCurrent(new ListVilleForm());
        }
        if (c == cmdEnregistrer) {
            Thread th = new Thread(this);
            th.start();
        }

    }

    public void run() {
        String streq1 = libelle.getString();
        String streq2 = pays.getString();
        String streq3 = description.getString();
        String streq4 = aeroport.getString();
        
        boolean result = new VilleDAO().insert(new Ville(streq1,streq2,streq3,streq4));
        Alert alert = new Alert("Résultat");
        if (result) {
            alert.setType(AlertType.CONFIRMATION);
            alert.setString("Ville ajoutée avec succés");
            Midlet.INSTANCE.disp.setCurrent(alert,new ListVilleForm());
        } else {
            alert.setType(AlertType.ERROR);
            alert.setString("Ajout de la ville échouée");
            Midlet.INSTANCE.disp.setCurrent(alert);
        }
    }

}
