/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esprit.govoyage.gui;
import com.esprit.govoyage.*;
import com.esprit.govoyage.test.Midlet;
import com.esprit.govoyage.dao.EvenementDAO;
import com.esprit.govoyage.entities.Evenement;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;

/**
 *
 * @author Lenovo-
 */
public class ListEvenetForm extends List implements CommandListener,Runnable {

    Command cmdAjout = new Command("Ajouter un evenement", Command.SCREEN, 0);
    Command cmdUpdate = new Command("Modifier un evenement", Command.SCREEN, 0);
    Command cmdDelete = new Command("Supprimer un evenement", Command.SCREEN, 0);
    Command cmdMap = new Command("Voir notre adresse sur la map", Command.SCREEN, 0);
    Command cmdExit = new Command("Exit", Command.EXIT, 0);


    public ListEvenetForm() {
        super("List Evenements", List.IMPLICIT);
        addCommand(cmdAjout);
        addCommand(cmdExit);
        addCommand(cmdUpdate);
        addCommand(cmdMap);
        //addCommand(cmdDelete);
        setCommandListener(this);
        Thread th = new Thread(this);
        th.start();
    }

   

    public void commandAction(Command c, Displayable d) {
        if (c == cmdAjout) {
            Midlet.INSTANCE.disp.setCurrent(new AddEvenetForm());
        }
        if (c == cmdUpdate) {
            Midlet.INSTANCE.disp.setCurrent(new UpdateEventForm());
        }
        if (c == cmdDelete) {
            Midlet.INSTANCE.disp.setCurrent(new AddEvenetForm());
        }
        if (c == cmdMap) {
           Midlet.INSTANCE.disp.setCurrent(new GoogleMapsMarkerCanvas(Midlet.INSTANCE, d));
        }
        if (c == cmdExit) {
            Midlet.INSTANCE.notifyDestroyed();
        }
    }

    public void run() {
        Evenement [] events = new EvenementDAO().select();
        if (events.length > 0) {
            for (int i = 0; i < events.length; i++) {
                append(events[i].getLibelle() + " - " + events[i].getDatedebut() +" - "+events[i].getPropriete(),null);
            }
        }
    }
}
